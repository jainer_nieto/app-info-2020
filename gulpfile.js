var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var htmlmin = require('gulp-htmlmin');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');


gulp.task('css', function(){

  return gulp.src('source/less/estilos.less')
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(gulp.dest('www/css'))
    
});


gulp.task('js', function () {
 
  return gulp.src('source/js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('www/js'));
    
});

gulp.task('html', function() {

  return gulp.src('source/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('www/'));

});

gulp.task('watch', function() {
  gulp.watch('source/less/*.less', ['css']);
  gulp.watch('source/less/componentes/*.less', ['css']);
  gulp.watch('source/js/*.js', ['js']);
  gulp.watch('source/*.html', ['html']);
});


//gulp.task('default', [ 'html', 'css', 'js' ]);
gulp.task('default', ['html', 'css', 'js', 'watch']);