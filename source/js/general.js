var URL_BASE = "https://infoschool.co/ingles_colegio";
var URL_FOTO = "files/fotos/";
var ID_DISPOSITIVO = "";
var SO = ""; //SISTEMA OPERATIVO
var TIMEOUT = 8000;
var storage = window.localStorage;
var nota_minima = 3.5;
var tus_usuario = localStorage.getItem("tipo_usuario");
var id_usuario = localStorage.getItem("id_usuario");
var id_colegio = localStorage.getItem("id_colegio");
// if (id_colegio == 1) {
//     var URL_CIRCULAR = "http://infoschool.co/infoschool-2018/infoschool/";    
// }
// if (id_colegio == 2) {
//     var URL_CIRCULAR = "http://infoschool.co/Salesiano_2018/";    
// }

function onLoad() {
   
    document.addEventListener("deviceready", onDeviceReady, false);
    console.log("ok");
}

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
    console.log("ready");
}

function onBackKeyDown() {
    console.log('tus_usuario:' + tus_usuario)
    if (tus_usuario == 38 || tus_usuario == 39) {
        var sw = false;
        console.log('backkeyDown')

        if ($('#page_asistencia').hasClass('page-active')) {
            ocultarPantalla('page_asistencia');
            sw = true;
            return false;
        }

        if ($('#page_asistencia_docente').hasClass('page-active')) {
            ocultarPantalla('page_asistencia_docente');
            sw = true;
            return false;
        }

        if ($('#page_listado_estudiante_sede').hasClass('page-active')) {
            ocultarPantalla('page_listado_estudiante_sede');
            sw = true;
            return false;
        }

        if(!sw){
            navigator.app.exitApp();
        }
    }
}


$(document).ready(function() {
    

	console.log("general");
    console.log("id_colegio: "+id_colegio);
	$('.js-menu').click(function() {
		$('#menu_principal').addClass('menu-active');
		$('.js-oscuro').addClass('visible')
	});

	$('.js-oscuro').click(function(event) {
		ocultarMenu();
	});

    $.ajaxSetup({
        timeout:15000,
        error:function( jqXHR, textStatus, errorThrown ) {
            console.log(jqXHR.status)
            if (jqXHR.status === 0) {
                if(textStatus!="timeout") {
                    
                    // navigator.notification.alert(
                    //     "Not connect: Verifique su conexion a internet.",  // message
                    //     function(){ },         // callback
                    //     'Error',            // title
                    //     'Aceptar'                  // buttonName
                    // );
                    console.log("fallo1");
                }else{
                    // navigator.notification.alert(
                    //     "Intentelo nuevamente",  // message
                    //     function(){ },         // callback
                    //     'Error',            // title
                    //     'Aceptar'                  // buttonName
                    // );
                    console.log("fallo");

                }
              } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
              } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
              } else if (textStatus === 'parsererror') {
                  //alert("Requested JSON parse failed.")
                console.log(textStatus+' : Requested JSON parse failed.');

              } else if (textStatus === 'timeout') {
                alert('Time out error.');
              } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
              } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
              }
        }
    });
    $('#page_ciudades .back').click(function(event) {
        ocultarPantalla('page_ciudades');
    });



    //LLENAR COMBOS PARA FECHA
    if ($('#dia')) {
        var dias = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"];

        for (var i = 0; i < dias.length; i++) {
            $('#dia').append('<option value="'+dias[i]+'">'+dias[i]+'</option>');    
        }

        var meses = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

        for (var i = 0; i < meses.length; i++) {
            $('#mes').append('<option value="'+meses[i]+'">'+meses[i]+'</option>');    
        }

        var ano = (new Date).getFullYear();

        for (var i = ano; i > ano-100; i--) {
            $('#ano').append('<option value="'+[i]+'">'+[i]+'</option>');    
        }
        

        $(".required").blur(function(event) {
            if ($(this).val()!="") {
                $(this).css('border', 'none');
            }else{
                $(this).css('border', '1px solid #da190b');
            }
            
        });
    }
    

});



function cerrarSesion(){
    localStorage.removeItem("id_usuario")
    localStorage.removeItem("tipo_usuario")
    localStorage.removeItem("id_colegio")
    window.location = "index.html"
}

function cargarGrados(pantalla){ // TRAE LOS CURSOS EN LOS QUE DA CLASES UN DOCENTE
    $.ajax({
        url: URL_BASE+'/notas/listar_grados',
        type: 'GET',
        dataType: 'json',
        data: {'id_usuario':id_usuario, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#'+pantalla+' .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {

        $("#"+pantalla+" #id_grado").html("");        
        $("#"+pantalla+" #id_grado").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#"+pantalla+" #id_grado").append('<option value="'+datos[i].id_grado+'">'+datos[i].grado+'</option>');
        }

        console.log("success");
        $('#'+pantalla+' .loader').html("");
    })
    .fail(function() {
        $('#'+pantalla+' .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarGrados(\""+pantalla+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function cargarCursosUnDocente(pantalla){ // TRAE LOS CURSOS EN LOS QUE DA CLASES UN DOCENTE
    $.ajax({
        url: URL_BASE+'/api/listarSalonesUnUsuario',
        type: 'POST',
        dataType: 'json',
        data: {'id_usuario':id_usuario},
        beforeSend: function() {
            $('#'+pantalla+' .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        console.log(datos)

        $("#"+pantalla+" #id_curso").html("");        
        $("#"+pantalla+" #id_curso").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#"+pantalla+" #id_curso").append('<option value="'+datos[i].id_salon+'">'+datos[i].nombre_salon+'</option>');
        }

        console.log("success");
        $('#'+pantalla+' .loader').html("");
    })
    .fail(function() {
        $('#'+pantalla+' .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarCursosUnDocente(\""+pantalla+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
        $('#'+pantalla+' .loader').html("");
    });
}

function listarSedesUsuario(pantalla){ // TRAE LOS CURSOS EN LOS QUE DA CLASES UN DOCENTE
    $.ajax({
        url: URL_BASE+'/api/listarSedesUsuario',
        type: 'POST',
        dataType: 'json',
        data: {'id_usuario':id_usuario},
        beforeSend: function() {
            $('#'+pantalla+' .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        console.log(datos)

        $("#"+pantalla+" #id_sede").html("");        
        $("#"+pantalla+" #id_sede").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#"+pantalla+" #id_sede").append('<option value="'+datos[i].id_sede+'">'+datos[i].nombre_sede+'</option>');
        }

        console.log("success");
        $('#'+pantalla+' .loader').html("");
    })
    .fail(function() {
        $('#'+pantalla+' .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarCursosUnDocente(\""+pantalla+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
        $('#'+pantalla+' .loader').html("");
    });
}



function cargarAsignaturas(id_curso, pantalla){ // TRAE LAS ASIGNATURAS DE UN CURSO Y UN DOCENTE
    if (pantalla=="page_notas_doc") {
        $('#rta_notas_doc').html("")
    }

    $.ajax({
        url: URL_BASE+'/notas/asignaturas_curso_docente',
        type: 'GET',
        dataType: 'json',
        data: {'id_usuario':id_usuario, 'id_curso':id_curso, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#'+pantalla+' .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
       
        $("#"+pantalla+" #id_asignatura").html("");        
        $("#"+pantalla+" #id_asignatura").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#"+pantalla+" #id_asignatura").append('<option value="'+datos[i].id_asignatura+'">'+datos[i].asignatura+'</option>');
        }
        console.log("success");
        $('#'+pantalla+' .loader').html("");
    })
    .fail(function() {
        $('#'+pantalla+' .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarAsignaturas("+id_curso+",\""+pantalla+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
    
}



function mostrarPantalla(pantalla){
	$('#'+pantalla).addClass('page-active');
	
	if ($('#menu_principal').hasClass('menu-active')) {
		$('#menu_principal').removeClass('menu-active');
		ocultarMenu();
	}
}

function ocultarPantalla(pantalla){
	$('#'+pantalla).removeClass('page-active');
}

function ocultarMenu(){
	$('#menu_principal').removeClass('menu-active');
	$('.js-oscuro').removeClass('visible')
}


function cargarCursosAdmin(pantalla){ // TRAE TODOS LOS CURSO
    $.ajax({
        url: URL_BASE+'/notas/cursos_admin',
        type: 'GET',
        dataType: 'json',
        data: {'id_usuario':id_usuario, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#'+pantalla+' .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {

        $("#"+pantalla+ " #id_curso").html("");        
        $("#"+pantalla+ " #id_curso").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#"+pantalla+ " #id_curso").append('<option value="'+datos[i].id_curso+'">'+datos[i].curso+'</option>');
        }
        console.log("success");
        $('#'+pantalla+' .loader').html("");
    })
    .fail(function() {
        $('#'+pantalla+' .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarCursosAdmin(\""+pantalla+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

