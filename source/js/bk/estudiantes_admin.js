function consultarEstudiante(){
	var term = $('#texto').val()

	$.ajax({
		url: URL_BASE+'/estudiante/busqueda',
		type: 'POST',
		dataType: 'json',
		data:{term:term, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_estudiantes .loader').html("<img src='img/cargando.svg' />");
        },
	})
	.done(function(datos) {
        $("#page-estudiante2").html("")
        var ul = ""
        ul +=  '<ul class="lista">'
            for (var i = 0; i < datos.length; i++) {
                ul += '<li onclick="datosEstudiante('+datos[i].id_estudiante+',\''+datos[i].nombre+' '+datos[i].curso+'\')">'+datos[i].nombre+" "+datos[i].curso+'<span class="next icon-next"></li>'
            }
        ul += '</ul>'
        
        $("#rta_estudiante").html(ul)
		$('#page_estudiantes .loader').html("");
	})
	.fail(function() {
		$('#page_estudiantes .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='consultarEstudiante()'></div>");
        console.log("error");
	})
	.always(function() {
		console.log("complete");
	});	
	
}

function datosEstudiante(id_estudiante, estudiante){
    mostrarPantalla("page_estudiante_det")
    $('#rta_anotaciones').css('display', 'block');
    $('#id_estudiante').val(id_estudiante)
    $('#estudiante').text(estudiante)

    $('#anotaciones').html("")

    $.ajax({
        url: URL_BASE+'/estudiante/datos_basicos_estudiante',
        type: 'GET',
        dataType: 'json',
        data:{'id_estudiante': id_estudiante, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_estudiante_det .loader').html("<img src='img/cargando.svg' />");
        },
    })
    .done(function(datos) {

        $('#nombre').text(estudiante)
        $('#id_estudiante').text(datos.id_estudiante)
        $('#identificacion').text("Id: "+datos.identificacion)
        $('#nacimiento').text("Nació: "+datos.nacimiento)
        $('#tel').text("Tel: "+datos.telefono)

        $('#padre').text("Padre: "+datos.padre)
        $('#telp').text("Tel: "+datos.telp)

        $('#madre').text("Madre: "+datos.madre)
        $('#telm').text("Tel: "+datos.telm)

        var tabla = "";
        var li = "";

        tabla += '<table width="100%" class="tabla">'
            tabla += '<tr>'
                tabla += '<th>Asignatura</th>'
                tabla += '<th>P1</th>'
                tabla += '<th>P2</th>'
                tabla += '<th>P3</th>'
                tabla += '<th>P4</th>'
                tabla += '<th>Acum</th>'
            tabla += '</tr>'

            
            for (var i = 0; i < datos.notas.length; i++) {
                tabla += '<tr>'
                    tabla += '<td>'+datos.notas[i].asignatura+'</td>'
                    tabla += '<td style="text-align: center;">'+datos.notas[i].nota1+'</td>'
                    tabla += '<td style="text-align: center;">'+datos.notas[i].nota2+'</td>'
                    tabla += '<td style="text-align: center;">'+datos.notas[i].nota3+'</td>'
                    tabla += '<td style="text-align: center;">'+datos.notas[i].nota4+'</td>'
                    tabla += '<td style="text-align: center;">'+datos.notas[i].acumulado+'</td>'
                tabla += '</tr>'
            }     
            
        tabla += '</table>'
        li += "<ul class='lista'>";
        if (datos.anotaciones.length > 0) {

            for (var j = 0; j < datos.anotaciones.length; j++) {
                li += '<li onclick="mostrarAnotacion('+datos.anotaciones[j].id_anotacion+')">'+datos.anotaciones[j].docente+' ['+datos.anotaciones[j].fecha+']</li>'
            }

        }else{
            li +="<h3 class='msg-observacion'>No hay registro de anotaciones</h3>";

        }
        li += "</ul>";

        $('#notas').html(tabla)
        $('#anotaciones').html(li)

        $('#page_estudiante_det .loader').html("");
  
    })
    .fail(function() {
        $('#page_estudiante_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='datosEstudiante("+id_estudiante+",\""+estudiante+"\")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}

function mostrarAnotacion(id_anotacion){
    mostrarPantalla("page_anotaciones_est");
    $.ajax({
        url: URL_BASE+'/anotaciones/cargar_anotaciones_detalle',
        type: 'POST',
        dataType: 'json',
        data:{'id_docente':id_usuario, 'id_anotacion': id_anotacion, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones_est .loader').html("<img src='img/cargando.svg' />");
        },
    })
    .done(function(datos) {
        $('#pga_estudiante').val(datos.nombre)
        $('#pga_fecha').val(datos.fecha)
        $('#pga_asunto').val(datos.asunto)
        $('#pga_descripcion').val(datos.descripcion)
        
        $('#page_anotaciones_est .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones_est .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='mostrarAnotacion("+id_anotacion+")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}


