$(document).ready(function() {
	$('#page-reprobadas2').css('display', 'none');
	console.log(localStorage.getItem("id_usuario"));
});
function cargarAsignaturasReprobadas(cod){
	var id_usuario = localStorage.getItem("id_usuario");
	$.ajax({
	url: URL_BASE+'/notas/cargar_asignaturas_reprobadas',
	type: 'GET',
	dataType: 'json',
	data: {'id_usuario':id_usuario, 'id_periodo':cod, 'nota_minima':nota_minima, 'id_colegio':id_colegio},
	beforeSend: function() {
	    	$('#page_reprobadas .loader').html("<img src='img/cargando.svg'/>");
	  	},
	})
	.done(function(datos) {
		var tabla = "";
		var resaltar_nota = "";

		tabla += '<table class="tabla tabla-azul mt20" border="1">'
			tabla += '<tr>'
				tabla += '<th>Asignatura</th>'
				tabla += '<th>Acumulado</th>'
			tabla += '<tr>'
			for (var i = 0; i < datos.length; i++) {
				tabla += '<tr>'
					tabla += '<td>'+datos[i].asignatura+'</td>'
					tabla += '<td style="text-align:right">'+datos[i].acumulado+'</td>'
				tabla += '<tr>'
			}
			
		tabla += '<table>'

		$('#rta_reprobadas1').html(tabla);
		console.log("success");
		$('#page_reprobadas .loader').html("");
	})
	.fail(function() {
		$('#page_reprobadas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarAsignaturasReprobadas("+cod+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}
