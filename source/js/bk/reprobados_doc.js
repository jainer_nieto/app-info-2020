function cargarReprobados(id_periodo){
	$.ajax({
		url: URL_BASE+'/notas/reprobadas_por_periodo_curso',
		type: 'POST',
		dataType: 'json',
		data:{'id_periodo':id_periodo, 'id_usuario':id_usuario, 'sw':'1', 'id_colegio':id_colegio},
		beforeSend: function() {
            $('#page_reprobados .loader').html("<img src='img/cargando.svg'/>");
        },
	})
	.done(function(datos) {
        $("#page-estudiante2").html("")
        var tabla = ""
       	
      
   			tabla += '<p class="msg-observacion text-center"><strong>'+datos.estudiantes.length+'</strong> Estudiantes reprobaron asignaturas</p>'
   		
   		
       	for (var i = 0; i < datos.estudiantes.length; i++) {

       		tabla += '<table class="tabla">'
       			tabla += '<tr>'
       				tabla += '<th colspan="2">'+datos.estudiantes[i].nombre+'</th>'
        		tabla += '</tr>'
        		for (var j = 0; j < datos.estudiantes[i].asignaturas.length; j++) {
        		
        			tabla += '<tr>'
	       				tabla += '<td style="width:90%">'+datos.estudiantes[i].asignaturas[j].materia+'</td>'
	       				tabla += '<td>'+datos.estudiantes[i].asignaturas[j].definitiva+'</td>'
	        		tabla += '</tr>'
        		}
        	tabla += '</table>'

       	}
       
        $("#rta_reprobados").html(tabla)
		$('#page_reprobados .loader').html("");
		//console.log("success");
	})
	.fail(function() {
		$('#page_reprobados .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarReprobados("+id_periodo+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});	
	
}