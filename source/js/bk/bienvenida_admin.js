$(document).ready(function() {
	obtenerDatos();
});

function obtenerDatos(){
 	$.ajax({
        url: URL_BASE+'/usuarios/get_admin',
        type: 'GET',
        dataType: 'json',
        data: {'id_usuario':id_usuario, 'id_colegio':id_colegio},
    })
    .done(function(datos) {

        $('#nombre_usuario_menu').text(datos.nombre)
        $('#email_usuario_menu').text(datos.email)

        console.log("success");
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}