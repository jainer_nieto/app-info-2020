$(document).ready(function() {
	
	cargarTareasEst()
	
});

function cargarTareasEst(){
	var id_estudiante = localStorage.getItem("id_usuario");
	$.ajax({
		url: URL_BASE+'/tarea/cargar_tareas_estudiante',
		type: 'GET',
		dataType: 'json',
		data: {'id_estudiante':id_estudiante, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_tareas .loader').html("<img src='img/cargando.svg' />");
	  	},
	})
	.done(function(datos) {
		
		var eventos = [];
		for (var i = 0; i < datos.length; i++) {
			eventos.push(
				{
					"title" : "Tarea", 
					"start" : datos[i].fecha,
					"end"   : datos[i].fecha
				}
			);
		}
			console.log(eventos);
			$('#calendar').fullCalendar({
			height: 320,
			dayClick: function(date, jsEvent, view) {

				 $('.fc-day').css('background-color', '#FFFFFF');
				 $(this).css('background-color', '#8BC34A');
				 fecha_tarea = date.format();
				 listTareasUnEstudiante(fecha_tarea);

		    },

		    eventClick: function(calEvent, jsEvent, view) {

		        fecha_tarea = calEvent.start._i.substring(0,10);
				listTareasUnEstudiante(fecha_tarea);
		        //alert(fecha_cita);
		    },
			header: {
				left: 'prev,next',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			navLinks: false, // can click day/week names to navigate views
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: eventos
		});
		$('#page_tareas .loader').html("");
	})
	.fail(function() {
		$('#page_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarTareasEst()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function listTareasUnEstudiante($fecha_tarea){
	$('#fecha').val(fecha_tarea)
	var id_usuario = localStorage.getItem("id_usuario");

	$.ajax({
		url: URL_BASE+'/tarea/list_tareas_un_estudiante',
		type: 'GET',
		dataType: 'json',
		data: {'id_estudiante':id_usuario, 'fecha_tarea':fecha_tarea, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_tareas .loader').html("<img src='img/cargando.gif' />");
	  	},
	})
	.done(function(datos) {
		var ul = "";

		ul += '<ul class="lista_cita">';
			for (var i = 0; i < datos.length; i++) {
				ul += '<li>'
				ul += '<h3>Título: '+datos[i].titulo+'</h3>';
				ul += '<p>Materia: '+datos[i].materia+'</p>';
				ul += '<span>Descripción: '+datos[i].descripcion+'</span>';
				ul +='</li>'
			}
		ul += '</ul>';

		$('#rta-tareas').html(ul);
		console.log("success");
		$('#page_tareas .loader').html("");
	})
	.fail(function() {
		$('#page_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='listTareasUnEstudiante("+fecha_tarea+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}