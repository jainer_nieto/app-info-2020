$(document).ready(function() {
  
  cargarComunicadosEst()
    
});

function cargarComunicadosEst(){
    $.ajax({
        url: URL_BASE+'/comunicado/cargar_comunicados',
        type: 'POST',
        dataType: 'json',
        data:{'id_estudiante':id_usuario, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_comunicados .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        $("#rta_comunicados").html("")

        var ul = ""
        ul +=  '<ul class="lista">'
            for (var i = 0; i < datos.length; i++) {
                ul += '<li onclick="mostrarComunicadoDetalle('+datos[i].id+')">'+datos[i].fecha+' '+datos[i].titulo
                ul += '<span class="next icon-next"></span>'
                ul +='</li>'
            }
        ul += '</ul>'
        
        $("#rta_comunicados").html(ul)
        $('#page_comunicados .loader').html("");
    })
    .fail(function() {
        $('#page_comunicados .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarComunicadosEst()'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}

function mostrarComunicadoDetalle(id_comunicado){
    mostrarPantalla("page_comunicados_det");
    $.ajax({
        url: URL_BASE+'/comunicado/cargar_comunicado_detalle',
        type: 'POST',
        dataType: 'json',
        data:{'id_comunicado':id_comunicado, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_comunicados_det .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        $("#cargue_comunicados").html("")

        $('#com_fecha').val(datos.fecha)
        $('#com_titulo').val(datos.titulo)
        $('#com_detalle').val(datos.detalle)
        $('#circular').attr("href", URL_CIRCULAR+"circulares/"+datos.pdf)
        
        $('#page_comunicados_det .loader').html("");

    })
    .fail(function() {
        $('#page_comunicados_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='mostrarComunicadoDetalle("+id_comunicado+")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}



