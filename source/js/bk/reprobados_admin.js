$(document).ready(function() {
    cargarCursosAdmin("page_reprobados")
});

function cargarReprobadosAdmin(){
	var id_periodo = $('#pr_id_periodo').val()
	var id_curso = $('#page_reprobados #id_curso').val()

	$.ajax({
		url: URL_BASE+'/notas/reprobadas_por_periodo_curso',
		type: 'POST',
		dataType: 'json',
		data:{'id_periodo':id_periodo, 'id_curso': id_curso, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_reprobados .loader').html("<img src='img/cargando.svg' />");
	    },
	})
	.done(function(datos) {
        
        var tabla = ""
       	
   		tabla += '<p class="msg-observacion text-center"><strong>'+datos.estudiantes.length+'</strong> Estudiantes reprobaron asignaturas</p>'
   		
   		
       	for (var i = 0; i < datos.estudiantes.length; i++) {

       		tabla += '<table class="tabla">'
       			tabla += '<tr>'
       				tabla += '<th colspan="2">'+datos.estudiantes[i].nombre+'</th>'
        		tabla += '</tr>'
        		for (var j = 0; j < datos.estudiantes[i].asignaturas.length; j++) {
        		
        			tabla += '<tr>'
	       				tabla += '<td style="width:90%">'+datos.estudiantes[i].asignaturas[j].materia+'</td>'
	       				tabla += '<td style="width:10%">'+datos.estudiantes[i].asignaturas[j].definitiva+'</td>'
	        		tabla += '</tr>'
        		}
        	tabla += '</table>'

       	}
       
        $("#rta-reprobados").html(tabla)
        $('#page_reprobados .loader').html("");

	})
	.fail(function() {
		$('#page_reprobados .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarReprobadosAdmin()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});	
	
}