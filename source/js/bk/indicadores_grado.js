var datos_grafica=[]
google.load("visualization", "1", {packages:["corechart"]});
$(document).ready(function() {
    cargarGrados("page_indicadores_grado")
});

function cargarIndicadoresGrado(){
	var id_periodo = $('#page_indicadores_grado #id_periodo').val()
	var id_grado = $('#page_indicadores_grado #id_grado').val()
	//mostrarPantalla("page_notas")
	var id_usuario = localStorage.getItem("id_usuario");
	$.ajax({
		url: URL_BASE+'/notas/cargar_indicadores_grado',
		type: 'GET',
		dataType: 'json',
		data: {'id_grado':id_grado, 'id_periodo':id_periodo, 'id_colegio':id_colegio},
		 beforeSend: function() {
	    	$('#page_indicadores_grado .loader').html("<img src='img/cargando.gif' />");
	  	},
	})
	.done(function(datos) {
		var tabla = ""

		tabla += '<table class="tabla" border="1" >'
			tabla += '<tr>'
				tabla += '<th rowspan="3">Asig.</th>'
				tabla += '<th colspan="8">Cant. estudiantes - '+datos[0].desempenos.cant_est+'</th>'
			tabla += '</tr>'
			tabla += '<tr>'
				tabla += '<th colspan="2">S</th>'
				tabla += '<th colspan="2">A</th>'
				tabla += '<th colspan="2">B</th>'
				tabla += '<th colspan="2">BJ</th>'
			tabla += '</tr>'
			tabla += '<tr>'
				tabla += '<th>C.</th>'
				tabla += '<th>%</th>'
				
				tabla += '<th>C.</th>'
				tabla += '<th>%</th>'

				tabla += '<th>C.</th>'
				tabla += '<th>%</th>'

				tabla += '<th>C.</th>'
				tabla += '<th>%</th>'
			tabla += '</tr>'

			for (var i = 0; i < datos[0].materia.length; i++) {
			
				tabla += '<tr>'
					tabla += '<td>'+datos[0].materia[i].asignatura+'</td>'
					tabla += '<td align="right">'+datos[0].materia[i].superior+'</td>'
					tabla += '<td align="right">'+datos[0].materia[i].prom_s+'%</td>'

					tabla += '<td align="right">'+datos[0].materia[i].alto+'</td>'
					tabla += '<td align="right">'+datos[0].materia[i].prom_a+'%</td>'

					tabla += '<td align="right">'+datos[0].materia[i].basico+'</td>'
					tabla += '<td align="right">'+datos[0].materia[i].prom_b+'%</td>'

					tabla += '<td align="right">'+datos[0].materia[i].bajo+'</td>'
					tabla += '<td align="right">'+datos[0].materia[i].prom_bj+'%</td>'
				tabla += '</tr>'
			
			}	
		tabla += '</table>'

		datos_grafica = datos[0].desempenos
		
		drawChartGrado();
      	
		$('#rta_indicadores_grado').html(tabla);
		console.log("success");
		$('#page_indicadores_grado .loader').html("");
	})
	.fail(function() {
		$('#page_indicadores_grado .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarIndicadoresGrado()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}


function drawChartGrado() {
	
	var data = google.visualization.arrayToDataTable([

		["Materia", "Cantidad", { role: "style" } ],

        ['S', parseInt(datos_grafica.cant_s), 'blue'],            // RGB value
        ['A', parseInt(datos_grafica.cant_a), 'silver'],            // English color name
        ['B', parseInt(datos_grafica.cant_b), 'gold'],
       	['BJ', parseInt(datos_grafica.cant_bj), 'red' ], // CSS-style declaration
       	 
      ]);

	var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: 'INDICADORES DE DESEMPEÑO POR GRADO',
        width: '100%',
        height: 400,
        bar: {groupWidth: "100%"},
        legend: { position: "none" },
		
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("rta_indicadores_grafica_grado"));
      chart.draw(view, options);
}



