$(document).ready(function() {
    
    cargarAnotacioneEstud()
    
});

function cargarAnotacioneEstud(){
    $.ajax({
        url: URL_BASE+'/anotaciones/cargar_anotaciones_un_estudiante',
        type: 'POST',
        dataType: 'json',
        data:{'id_estudiante':id_usuario, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones .loader').html("<img src='img/cargando.gif' />");
        },
    })
    .done(function(datos) {
        $('#texto_anotaciones').removeClass("visible").addClass('hidden')   
        $("#rta_anotaciones").html("")
        if (datos.length>0) {
            var ul =  '<ul class="lista">'
                for (var i = 0; i < datos.length; i++) {
                    ul += '<li onclick="mostrarAnotacion('+datos[i].id+')">'+datos[i].asunto
                    ul += '<span class="next icon-next"></span>'
                    ul +='</li>'
                }
            ul += '</ul>'
            
            $("#rta_anotaciones").html(ul)
        }else{
            $('#texto_anotaciones').removeClass("hidden").addClass('visible')
        }
        $('#page_anotaciones .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarAnotacioneEstud()'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function mostrarAnotacion(id_anotacion){
    mostrarPantalla('page_anotaciones_det');
    $.ajax({
        url: URL_BASE+'/anotaciones/cargar_anotaciones_un_estudiante_detalle',
        type: 'POST',
        dataType: 'json',
        data:{'id_anotacion':id_anotacion, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones_det .loader').html("<img src='img/cargando.gif' />");
        },
    })
    .done(function(datos) {
        $('#docente').val(datos.docente)
        $('#fecha').val(datos.fecha)
        $('#asunto').val(datos.asunto)
        $('#descripcion').val(datos.descripcion)

        $('#page_anotaciones_det .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='mostrarAnotacion("+id_anotacion+")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}



