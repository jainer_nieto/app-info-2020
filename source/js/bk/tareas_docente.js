var fecha_tarea = new Date().toJSON().slice(0,10);
$(document).ready(function() {
	listarTareas();
	
});

function listarTareas(){

	$('#page-tareas2').css('display', 'none');
	var id_docente = localStorage.getItem("id_usuario");

	$.ajax({
		url: URL_BASE+'/tarea/cargar_tareas',
		type: 'GET',
		dataType: 'json',
		data: {'id_docente':id_docente, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_tareas .loader').html("<img src='img/cargando.svg'/>");
	  	},
	})
	.done(function(datos) {
		
		var eventos = [];
		for (var i = 0; i < datos.length; i++) {
			eventos.push(
				{
					"title" : "Tarea", 
					"start" : datos[i].fecha,
					"end"   : datos[i].fecha
				}
			);
		}
			console.log(eventos);
			$('#calendar').fullCalendar({
			dayClick: function(date, jsEvent, view) {
				 $('.fc-day').css('background-color', '#FFFFFF');
				 $(this).css('background-color', '#8BC34A');
				 fecha_tarea = date.format();
				 listTareasUnDocente(fecha_tarea);
		    },

		    eventClick: function(calEvent, jsEvent, view) {
		        fecha_tarea = calEvent.start._i.substring(0,10);
				listTareasUnDocente(fecha_tarea);
		        //alert(fecha_cita);
		    },
			header: {
				left: 'prev,next',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			navLinks: false, // can click day/week names to navigate views
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: eventos
		});

		$('#page_tareas .loader').html("");
		
	})
	.fail(function() {
		$('#page_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='listarTareas()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}

function clearCajas(){
	$('#id_asignatura').val("");
	$('#titulo').val("");
	$('#descripcion').val("");
	
	
}

function asignarTarea(){
	mostrarPantalla("page_crear_tareas")
	clearCajas();
	$('#fecha').val(fecha_tarea)
	var id_usuario = localStorage.getItem("id_usuario");

	$.ajax({
		url: URL_BASE+'/notas/cursos_docente',
		type: 'GET',
		dataType: 'json',
		data: {'id_usuario':id_usuario, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_crear_tareas .loader').html("<img src='img/cargando.svg'/>");
	  	},
	})
	.done(function(datos) {
		var select = "";

		$("#id_curso").html("");        
        $("#id_curso").append('<option value="">Seleccione..</option>');
        for (i = 0; i < datos.length; i++){
            $("#id_curso").append('<option value="'+datos[i].id_curso+'">'+datos[i].curso+'</option>');
        }
		console.log("success");
		$('#page_crear_tareas .loader').html("");
	})
	.fail(function() {
		$('#page_crear_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='asignarTarea()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}

function listTareasUnDocente($fecha_tarea){
	$('#fecha').val(fecha_tarea)
	var id_usuario = localStorage.getItem("id_usuario");

	$.ajax({
		url: URL_BASE+'/tarea/list_tareas_un_docente',
		type: 'GET',
		dataType: 'json',
		data: {'id_docente':id_usuario, 'fecha_tarea':fecha_tarea, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_tareas .loader').html("<img src='img/cargando.svg'/>");
	  	},
	})
	.done(function(datos) {
		var ul = "";

		ul += '<ul class="lista_cita">';
			for (var i = 0; i < datos.length; i++) {
				ul += '<li>'
				ul += '<h3>Título: '+datos[i].titulo+'</h3>';
				ul += '<p>Curso: '+datos[i].curso+'</p>';
				ul += '<p>Materia: '+datos[i].materia+'</p>';
				ul += '<span>Descripción: '+datos[i].descripcion+'</span>';

				ul += '<div class="overflow-h">'
					// ul +='<button type="button" class="btn-clear color_primary left" onclick="ShowEditTarea('+datos[i].id_tarea+')">Editar</button>'
					ul +='<button type="button" class="btn-clear color_delete right" onclick="deleteTarea('+datos[i].id_tarea+')">Eliminar</button>'
				ul +='</div>'

				ul +='</li>'
			}
		ul += '</ul>';


		$('#rta_tarea').html(ul);
		console.log("success");
		$('#page_tareas .loader').html("");
		
	})
	.fail(function() {
		$('#page_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='listTareasUnDocente()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	
}

/*function ShowEditTarea(id_tarea){
	mostrarPantalla("page_edit_tarea")

}*/


function deleteTarea(id_tarea){

	$.ajax({
		url: URL_BASE+'/tarea/eliminar',
		type: 'POST',
		dataType: 'json',
		data: {
			id_tarea:id_tarea, 
			
		},
	})
	.done(function(datos) {
		if (datos.estado == "1") {
			$('#rta_tarea').html("");
			$('#calendar').fullCalendar( 'destroy' )
			console.log("registro eliminado");
			listarTareas();
		}
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}


function guardarTarea(){
	var fecha = $('#fecha').val()
	var id_curso = $('#id_curso').val()
	var id_asignatura = $('#id_asignatura').val()
	var titulo = $('#titulo').val()
	var descripcion = $('#descripcion').val()
	var id_docente = localStorage.getItem("id_usuario");

	if (id_curso == "") {
		alert("Debe seleccionar un curso")
		return
	}

	if (descripcion == "") {
		alert("Debe agregar una descripcion")
		return
	}

	$.ajax({
		url: URL_BASE+'/tarea/guardar',
		type: 'POST',
		dataType: 'json',
		data: {
			'id_docente':id_docente, 
			'id_curso':id_curso,
			'id_asignatura':id_asignatura,
			'fecha':fecha,
			'descripcion':descripcion,
			'titulo':titulo,
			'id_colegio':id_colegio
		},
		beforeSend: function() {
	    	$('#page_crear_tareas .loader').html("<img src='img/cargando.svg'/>");
	  	},
	})
	.done(function(datos) {
		if (datos.estado == 1) {
			ocultarPantalla("page_crear_tareas");
			$('#calendar').fullCalendar( 'destroy' )
			listarTareas();
		}
		console.log("success");
		$('#page_crear_tareas .loader').html("");
	})
	.fail(function() {
		$('#page_crear_tareas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='guardarTarea()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}