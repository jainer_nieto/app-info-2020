$(document).ready(function() {
   listarAnotacionesDocente();
    
});

function listarAnotacionesDocente(){
    $.ajax({
        url: URL_BASE+'/anotaciones/cargar_anotaciones',
        type: 'POST',
        dataType: 'json',
        data:{'id_docente':id_usuario, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        $("#rta_anotaciones").html("")
        if (datos.length > 0) {
            var ul = ""
            ul +=  '<ul class="lista">'
                for (var i = 0; i < datos.length; i++) {
                    ul += '<li onclick="mostrarAnotacion('+datos[i].id_anotacion+')">'+datos[i].nombre+' - '+datos[i].curso
                    ul += '<span class="next icon-next"></span>'
                    ul +='</li>'
                }
            ul += '</ul>'
        }else{
            var ul = '<div id="texto_anotaciones" class="content_page w90 box_center"><h3 class="text_vacio">No hay registro de anotaciones</h3></div>';
        }
        
        $("#rta_anotaciones").html(ul)
        $('#page_anotaciones .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='listarAnotacionesDocente()'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}



function consultarEstudiante(){
	var term = $('#texto').val()

	$.ajax({
		url: URL_BASE+'/estudiante/busqueda',
		type: 'POST',
		dataType: 'json',
		data:{term:term, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones_nuevo .loader').html("<img src='img/cargando.svg'/>");
        },
	})
	.done(function(datos) {
        
      

        var ul = ""
        ul +=  '<ul class="lista">'
            for (var i = 0; i < datos.length; i++) {
                ul += '<li onclick="datosEstudiante('+datos[i].id_estudiante+',\''+datos[i].nombre+' '+datos[i].curso+'\')">'+datos[i].nombre+" - "+datos[i].curso
                ul += '<span class="next icon-next"></span>'
                ul +='</li>'
            }
        ul += '</ul>'
        
        $("#rta_estudiante").html(ul)
		$('#page_anotaciones_nuevo .loader').html("");
	})
	.fail(function() {
		$('#page_anotaciones_nuevo .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='consultarEstudiante()'></div>");
        console.log("error");
	})
	.always(function() {
		console.log("complete");
	});	
	
}

function datosEstudiante(id_estudiante, estudiante){
    mostrarPantalla("page_anotaciones_nuevo_det")
    $('#id_estudiante').val(id_estudiante)
    $('#and_estudiante').val(estudiante)
}

function enviarAnotacion(){
    id_estudiante = $('#id_estudiante').val();
    asunto = $('#and_asunto').val();
    descripcion = $('#and_descripcion').val();
    $.ajax({
        url: URL_BASE+'/anotaciones/guardar',
        type: 'POST',
        dataType: 'json',
        data:{
                'id_estudiante':id_estudiante,
                'id_usuario':id_usuario,
                'asunto':asunto,
                'descripcion':descripcion,
                'id_colegio':id_colegio
            },
        beforeSend: function() {
            $('#page_anotaciones_nuevo_det .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        if (datos.estado=='1') {
            alert ("Mensaje enviado");
            ocultarPantalla("page_anotaciones_nuevo_det");
            $('#and_asunto').val("")
            $('#and_descripcion').val("")
            listarAnotacionesDocente();
        }
        $('page_anotaciones_nuevo_det .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones_nuevo_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='enviarAnotacion()'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}

function mostrarAnotacion(id_anotacion){
    mostrarPantalla("page_anotaciones_det");
    $.ajax({
        url: URL_BASE+'/anotaciones/cargar_anotaciones_detalle',
        type: 'POST',
        dataType: 'json',
        data:{'id_docente':id_usuario, 'id_anotacion': id_anotacion, 'id_colegio':id_colegio},
        beforeSend: function() {
            $('#page_anotaciones_det .loader').html("<img src='img/cargando.svg'/>");
        },
    })
    .done(function(datos) {
        $("#cargue_anotaciones").html("")

        $('#ad_estudiante').val(datos.nombre);
        $('#ad_fecha').val(datos.fecha);
        $('#ad_asunto').val(datos.asunto);
        $('#ad_descripcion').val(datos.descripcion);

       
        $('#page_anotaciones_det .loader').html("");
    })
    .fail(function() {
        $('#page_anotaciones_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='mostrarAnotacion("+id_anotacion+")'></div>");
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    }); 
}


