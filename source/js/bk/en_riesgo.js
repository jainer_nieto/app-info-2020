function cargarReprobadosAcumuladoDoc(id_periodo){

	if (id_periodo=="") {
		alert("Debe seleccionar un periodo")
		return false
	}

	$.ajax({
		url: URL_BASE+'/notas/cargar_acumulados_reprobados_curso_periodo',
		type: 'POST',
		dataType: 'json',
		data:{'id_periodo':id_periodo, 'id_usuario': id_usuario, 'sw':'1', 'id_colegio':id_colegio },
		beforeSend: function() {
            $('#page_rep_acum .loader').html("<img src='img/cargando.svg'/>");
        },
	})
	.done(function(datos) {
        $("#page-estudiante2").html("")
        var tabla = ""
       	
       	
   		tabla += '<p class="msg-observacion text-center"><strong>'+datos.estudiantes.length+'</strong> Estudiantes con asignaturas reprobadas en su acumulado</p>'
   		
   		
       	for (var i = 0; i < datos.estudiantes.length; i++) {

       		tabla += '<table class="tabla">'
       			tabla += '<tr>'
       				tabla += '<th colspan="2">'+datos.estudiantes[i].nombre+'</th>'
        		tabla += '</tr>'
        		for (var j = 0; j < datos.estudiantes[i].asignaturas.length; j++) {
        		
        			tabla += '<tr>'
	       				tabla += '<td style="width:90%;">'+datos.estudiantes[i].asignaturas[j].asignatura+'</td>'
	       				tabla += '<td>'+datos.estudiantes[i].asignaturas[j].acumulado+'</td>'
	        		tabla += '</tr>'
        		}
        	tabla += '</table>'

       	}
       
        $("#rta_reprobadas1").html(tabla)
        $('#page_rep_acum .loader').html("");
		
	})
	.fail(function() {
		$('#page_rep_acum .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarReprobadosAcumuladoDoc("+id_periodo+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});	
	
}