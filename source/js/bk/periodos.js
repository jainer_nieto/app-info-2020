$(document).ready(function() {
	listarPeriodos()
});

function listarPeriodos(){
	$.ajax({
	url: URL_BASE+'/periodo/cargar_periodos',
	type: 'GET',
	dataType: 'json',
	data: {'id_usuario':id_usuario, 'id_colegio':id_colegio},
	beforeSend: function() {
            $('#page_periodos .loader').html("<img src='img/cargando.svg'/>");
        },
	})
	.done(function(datos) {
		var ul = "";
	
		for (var i = 0; i < datos.length; i++) {

			ul += '<ul class="lista">';
				ul += '<li onclick="editarPeriodo('+datos[i].id+')">  '+datos[i].per_letras+'</li>';
			ul += '</ul>';
		}

		$('#rta-periodos').html(ul);
		console.log("success");
		$('#page_periodos .loader').html("");
	})
	.fail(function() {
		$('#page_periodos .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='listarPeriodos()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function editarPeriodo(id_periodo){
	mostrarPantalla("page_periodo_det");
	$('#page_periodo').html("")
	$.ajax({
	url: URL_BASE+'/periodo/editar_periodo',
	type: 'GET',
	dataType: 'json',
	data: {'id_periodo':id_periodo, 'id_colegio':id_colegio},
	beforeSend: function() {
    	$('#page_periodo_det .loader').html("<img src='img/cargando.svg' />");
    },
	})
	.done(function(datos) {
		var tabla = "";

		$('#inicio').val(datos.inicio)
		$('#fin').val(datos.fin)
		$('#id_periodo').val(datos.id)

		console.log("success");
		$('#page_periodo_det .loader').html("");
	})
	.fail(function() {
		$('#page_periodo_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='editarPeriodo("+id_periodo+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function actualizarPeriodo(){
	var inicio = $('#inicio').val()
	var fin = $('#fin').val()
	var id_periodo = $('#id_periodo').val()

	$.ajax({
	url: URL_BASE+'/periodo/actualizar_periodo',
	type: 'POST',
	dataType: 'json',
	data: {'id_periodo':id_periodo, 'inicio':inicio, 'fin':fin, 'id_colegio':id_colegio},
	beforeSend: function() {
        $('#page_periodo_det .loader').html("<img src='img/cargando.svg'/>");
    },
	})
	.done(function(datos) {
		if(datos.estado==1){
			alert("Registro actualizado")
		}else{
			alert("No fué posible actualizar el registro. Consulte con soporte técnico")
		}
		console.log("success");
		$('#page_periodo_det .loader').html("");
	})
	.fail(function() {
		$('#page_periodo_det .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='actualizarPeriodo()'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

