var id_periodo = "";
$(document).ready(function() {
	$('#rta_notas2').css('display', 'none');
	console.log(localStorage.getItem("id_usuario"));
});
function cargarAsignaturasConNotas(cod){
	id_periodo = cod;
	mostrarPantalla("page_notas")
	var id_usuario = localStorage.getItem("id_usuario");
	$.ajax({
		url: URL_BASE+'/notas/cargar_asignaturas',
		type: 'GET',
		dataType: 'json',
		data: {'id_usuario':id_usuario, 'id_periodo':cod, 'id_colegio':id_colegio},
		 beforeSend: function() {
	    	$('#page_notas .loader').html("<img src='img/cargando.gif' />");
	  	},
	})
	.done(function(datos) {
		var select = "";
		var resaltar_nota = "";
		var perdido = "";
		select += '<ul class="lista2">';
		for (var i = 0; i < datos.length; i++) {
			// if (datos[i].def<nota_minima) {
			// 	resaltar_nota = nota_perdida
			// }
			puntos = "";
			if (datos[i].asignatura.length>=35) {
				puntos = "...";
			}
			perdido = "";
			if (datos[i].def<nota_minima) {
				perdido  = "perdido";
			}

			if (i == 0) {
				select += '<li class="promedio">PROMEDIO <p>'+datos[0].promedio+'</p> ';
			}

			
			select += '<li class="'+perdido+'" onclick="notasAsignatura('+datos[i].id_asignatura+')">'+datos[i].asignatura.substring(0, 35)+puntos+'<p>'+datos[i].def+'</p> <span class="next icon-next"></span></li>';
			
		}
		select += '</ul>';

		$('#rta_notas1').html(select);
		console.log("success");
		$('#page_notas .loader').html("");
	})
	.fail(function() {
		$('#page_notas .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarAsignaturasConNotas("+cod+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function notasAsignatura(cod){
	mostrarPantalla("page_notas_detalle")
	var id_usuario = localStorage.getItem("id_usuario");
	
	$.ajax({
		url: URL_BASE+'/notas/notas_asignatura',
		type: 'GET',
		dataType: 'json',
		data: {'id_asignatura':cod, 'id_periodo':id_periodo, 'id_usuario':id_usuario, 'id_colegio':id_colegio},
		beforeSend: function() {
	    	$('#page_notas_detalle .loader').html("<img src='img/cargando.gif' />");
	  	},
	})
	.done(function(datos) {
		var ul = "";
		$('#page-notas1').css('display', 'none');
		$('#rta_notas2').css('display', 'block');

		ul += '<p class="msg-observacion">Se debe tener en cuenta que las notas que se observan en este informe son el acumulado de las notas que el docente a ingresado hasta la fecha, por lo tanto estan sugetas a cambios</p>'
		ul += '<h1 style="text-align:center">'+datos[0].materia+'</h1>';
		
		for (var i = 0; i < datos.length; i++) {
			ul += '<ul>'
				ul += '<li><p class="desempeno">DESEMPEÑO SOCIOAFECTIVO 20%</p>'
					ul += '<table width="100%">'

						if (datos[i].n1!="0.0") {
							
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo1+'">Nota 1</td>'
								if (datos[i].n1<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n1+'</td>'
								}else{
									ul += '<td>'+datos[i].n1+'</td>'
								}
							ul += '</tr>'
							
						}

						if (datos[i].n2!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo2+'">Nota 2</td>'
								if (datos[i].n2<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n2+'</td>'
								}else{
									ul += '<td>'+datos[i].n2+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n3!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo3+'">Nota 3</td>'
								if (datos[i].n3<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n3+'</td>'
								}else{
									ul += '<td>'+datos[i].n3+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n4!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo4+'">Nota 4</td>'
								if (datos[i].n4<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n4+'</td>'
								}else{
									ul += '<td>'+datos[i].n4+'</td>'
								}
							ul += '</tr>'
						}

						ul += '<tr>'
							ul += '<th>DEFINITIVA DESEMPEÑO</th>'
							if (datos[i].def1<nota_minima) {
								ul += '<th class="nota_perdida"><strong>'+datos[i].def1+'</strong></th>'
							}else{
								ul += '<th><strong>'+datos[i].def1+'</strong></th>'
							}
						ul += '</tr>'

					ul += '</table>'
				ul += '</li>'

				ul += '<li><p class="desempeno">DESEMPEÑO COGNITIVO 40%</p>'
					ul += '<table width="100%">'

						if (datos[i].n5!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo5+'">Nota 1</td>'
								if (datos[i].n5<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n5+'</td>'
								}else{
									ul += '<td>'+datos[i].n5+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n6!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo6+'">Nota 2</td>'
								if (datos[i].n6<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n6+'</td>'
								}else{
									ul += '<td>'+datos[i].n6+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n7!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo7+'">Nota 3</td>'
								if (datos[i].n7<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n7+'</td>'
								}else{
									ul += '<td>'+datos[i].n7+'</td>'
								}
							ul += '</tr>'
						}

						ul += '<tr>'
							ul += '<th>DEFINITIVA DESEMPEÑO</th>'
							if (datos[i].def2<nota_minima) {
									ul += '<th class="nota_perdida"><strong>'+datos[i].def2+'</strong></th>'
								}else{
									ul += '<th><strong>'+datos[i].def2+'</strong></th>'
								}
						ul += '</tr>'

					ul += '</table>'
				ul += '</li>'

				ul += '<li><p class="desempeno">DESEMPEÑO PROCEDIMENTAL 40%</p>'
					ul += '<table width="100%">'

						if (datos[i].n8!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo8+'">Nota 1</td>'
								if (datos[i].n8<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n8+'</td>'
								}else{
									ul += '<td>'+datos[i].n8+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n9!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo9+'">Nota 2</td>'
								if (datos[i].n9<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n9+'</td>'
								}else{
									ul += '<td>'+datos[i].n9+'</td>'
								}
							ul += '</tr>'
						}

						if (datos[i].n10!="0.0") {
							ul += '<tr>'
								ul += '<td title="'+datos[i].titulo10+'">Nota 3</td>'
								if (datos[i].n10<nota_minima) {
									ul += '<td class="nota_perdida">'+datos[i].n10+'</td>'
								}else{
									ul += '<td>'+datos[i].n10+'</td>'
								}
							ul += '</tr>'
						}

						ul += '<tr>'
							ul += '<th>DEFINITIVA DESEMPEÑO</th>'
							if (datos[i].n7<nota_minima) {
									ul += '<th class="nota_perdida"><strong>'+datos[i].def3+'</strong></th>'
								}else{
									ul += '<th><strong>'+datos[i].def3+'</strong></th>'
								}
						ul += '</tr>'

					ul += '</table>'
				ul += '</li>'
			ul += '</ul>'
		}
		if (datos[0].def<nota_minima) {
			ul += '<h1 class="bg-1">DEFINITIVA A FECHA DE HOY: <strong class="nota_perdida">'+datos[0].def+'</strong></h1>'
			ul += '<p class="msg-observacion">Para el próximo periodo la nota mìnima que debes obtener en esta asignatura es <strong>'+datos[0].nota_sugerida+'</strong></p>'
		}else{
			ul += '<h1 class="bg-1">DEFINITIVA A FECHA DE HOY: <strong>'+datos[0].def+'</strong></h1>'
		}
		
		$('#rta_notas2').html(ul);
		console.log("success");
		$('#page_notas_detalle .loader').html("");
	})
	.fail(function() {
		$('#page_notas_detalle .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='notasAsignatura("+cod+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}