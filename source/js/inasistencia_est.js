$(document).ready(function() {
	//$('#page-notas2').css('display', 'none');
	cargarCursosUnDocente('page_asistencia');
	console.log(localStorage.getItem("id_usuario"));
});

function cambiarFecha(fecha) {
	$('#page_asistencia_rta').html("")
	$("#id_curso").val("");
	if(fecha == ""){
		alert('Seleccione fecha')
		// navigator.notification.alert(
		// 	"Seleccione fecha",  // message
		// 	function(){ },         // callback
		// 	'Error',            // title
		// 	'Aceptar'                  // buttonName
		// );
		return false;
	}
}

function cargarEstudiantes(id_salon){
	$('#page_asistencia_rta').html("")

	var id_usuario = localStorage.getItem("id_usuario");
	var fecha = $("#page_asistencia_fecha").val()

	if(id_salon == ""){
		alert('Seleccione salon')
		// navigator.notification.alert(
		// 	"Seleccione salon",  // message
		// 	function(){ },         // callback
		// 	'Error',            // title
		// 	'Aceptar'                  // buttonName
		// );
		return false;
	}

	if(fecha == ""){
		alert('Seleccione fecha')
		// navigator.notification.alert(
		// 	"Seleccione fecha",  // message
		// 	function(){ },         // callback
		// 	'Error',            // title
		// 	'Aceptar'                  // buttonName
		// );
		return false;
	}

	

	$.ajax({
		url: URL_BASE+'/api/cargar_estudiante_asistencia',
		type: 'POST',
		dataType: 'json',
		data: {'id_salon':id_salon, fecha:fecha},
		beforeSend: function() {
		    $('#page_asistencia .loader').html("<img src='img/cargando.svg'/>");
		},
	})
	.done(function(datos) {
		console.log(datos)

		var tabla ='';
        tabla += '<p style="font-size:12px;">*Solo debe seleccionar a los estudiantes que NO asistieron y dar clic en el boton "Guardar Asistencia"</p><center><table class="lista">';
        tabla += '<tr>'
        tabla += '<th class="tac">No.</th>'
        //tabla += '<th class="tac">Identificación</th>'
        tabla += '<th class="tac">Apellidos y Nombres</th>'
        tabla += '<th class="tac">Inasistencia</th>'
		
       
        tabla += '</tr>'
        
        var checked = "";
        for (var i = 0; i < datos.length; i++) {

            checked = (datos[i].asistio) ? "checked" : "";
           
            tabla += '<tr>'
                tabla += '<td>'+(i+1)+'</td>'
                //tabla += '<td class="tal">'+datos[i].numero_documento+'</td>'
                tabla += '<td>'+datos[i].estudiante+'</td>'
                
                tabla += '<td class="text-center"> <input type="hidden" name="hdn_estudiante[]" value="'+datos[i].id_estudiante+'"/>  <input type="checkbox" '+checked+' name="chk[]" value="'+datos[i].id_estudiante+'"/>   </td>'
            tabla += '</tr>'; 
            
        }

		tabla += '</table></center><br>';
		botones = '<button type="button" class="btn btn_full bg_accent mt20" onclick="guardarAsistencia();">Guardar Asistencia</button>';

        $('#page_asistencia_rta').html("")
	    $("#page_asistencia_rta").html(tabla + botones)
		

	})
	.fail(function() {
		$('#page_asistencia .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarEstudiantes("+id_salon+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_asistencia .loader').html("");
	});
}

function guardarAsistencia(){

	$('#page_asistencia .loader').html("<img src='img/cargando.svg'/>");

    var info = $('#page_asistencia_frm').serialize();
	$.ajax({
		url: URL_BASE+"/api/guardar_asistencia",
		type: 'POST',
		dataType: 'json',
		data:info,
	})
	.done(function(datos) {

	if(datos.estado=="1"){
		alert(datos.mensaje)
	}else{
		alert(datos.mensaje)
	}
	console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_asistencia .loader').html("");
		
	});
	
}


