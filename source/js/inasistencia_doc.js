$(document).ready(function() {
	//$('#page-notas2').css('display', 'none');
	listarSedesUsuario('page_asistencia_docente');
	//console.log(localStorage.getItem("id_usuario"));
});

function cambiarFecha(fecha) {
	$('#page_asistencia_docente_rta').html("")
	$("#id_sede").val("");
	if(fecha == ""){
		alert('Seleccione fecha')
		// navigator.notification.alert(
		// 	"Seleccione fecha",  // message
		// 	function(){ },         // callback
		// 	'Error',            // title
		// 	'Aceptar'                  // buttonName
		// );
		return false;
	}
}


function cargarDocentes(id_sede){

    var id_usuario = localStorage.getItem("id_usuario");
    var fecha = $("#page_asistencia_docente_fecha").val()
    
    $('#page_asistencia_docente_rta').html("")
    
    if(id_sede == ""){
		alert('Seleccione sede')
		return false;
    }
    
    if(fecha == ""){
		alert('Seleccione fecha')
	
		return false;
	}
	

  	if (id_sede == "") {
        alert("Seleccione salon")
        return false;
    }


	$.ajax({
	    url: URL_BASE+'/api/cargarDocentesAsistencia',
	    type: 'POST',
	    dataType: 'json',
        data: {id_sede: id_sede, fecha:fecha},
        beforeSend: function() {
		    $('#page_asistencia_docente .loader').html("<img src='img/cargando.svg'/>");
		},
	})
  	
  	.done(function(datos) {
		
		console.log(datos)

		var tabla ='';
        tabla += '<p style="font-size:12px;>*Solo debe seleccionar a los docentes que NO asistieron y dar clic en el boton "Guardar Asistencia"</p><center><table class="lista">';
        tabla += '<tr>'
        tabla += '<th class="tac">No.</th>'
        //tabla += '<th class="tac">Identificación</th>'
        tabla += '<th class="tac">Apellidos y Nombres</th>'
        tabla += '<th class="tac">Inasistencia</th>'
		
       
        tabla += '</tr>'
        
        var checked = "";
        for (var i = 0; i < datos.length; i++) {

            checked = (datos[i].asistio) ? "checked" : "";
           
            tabla += '<tr>'
                tabla += '<td>'+(i+1)+'</td>'
                //tabla += '<td class="tal">'+datos[i].numero_documento+'</td>'
                tabla += '<td>'+datos[i].nombre_docente+'</td>'
                
                tabla += '<td class="text-center"> <input type="hidden" name="hdn_docente[]" value="'+datos[i].id_docente+'"/>  <input type="checkbox" '+checked+' name="chk[]" value="'+datos[i].id_docente+'"/>   </td>'
            tabla += '</tr>'; 
            
        }

		tabla += '</table></center><br>';
		botones = '<button type="button" class="btn btn_full bg_accent mt20" onclick="guardarAsistenciaDocente();">Guardar Asistencia</button>';

        $('#page_asistencia_docente_rta').html("")
	    $("#page_asistencia_docente_rta").html(tabla + botones)
	})
	.fail(function() {
		$('#page_asistencia_docente .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarDocentes("+id_sede+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_asistencia_docente .loader').html("");
	});
  	
}

function guardarAsistencia(){

	$('#page_asistencia .loader').html("<img src='img/cargando.svg'/>");

    var info = $('#page_asistencia_frm').serialize();
	$.ajax({
		url: URL_BASE+"/api/guardar_asistencia",
		type: 'POST',
		dataType: 'json',
		data:info,
	})
	.done(function(datos) {

	if(datos.estado=="1"){
		alert(datos.mensaje)
	}else{
		alert(datos.mensaje)
	}
	console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_asistencia .loader').html("");
		
	});
	
}

function guardarAsistenciaDocente(){

	$('#page_asistencia_docente .loader').html("<img src='img/cargando.svg'/>");

    var info = $('#page_asistencia_docente_frm').serialize();
	$.ajax({
		url: URL_BASE+"/api/guardar_asistencia_docente",
		type: 'POST',
		dataType: 'json',
		data:info,
	})
	.done(function(datos) {

	if(datos.estado=="1"){
		alert(datos.mensaje)
	}else{
		alert(datos.mensaje)
	}
	console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_asistencia_docente .loader').html("");
		
	});
	
}


