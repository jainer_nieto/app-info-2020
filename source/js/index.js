$(document).ready(function() {
	if (localStorage.tipo_usuario) {
		// if (localStorage.tipo_usuario==26) {
		// 	window.location = "bienvenida_est.html";
		// }else if(localStorage.tipo_usuario == 16 || localStorage.tipo_usuario == 24 || localStorage.tipo_usuario == 32){
		// 	window.location = "bienvenida_doc.html";
		// }else{
		// 	window.location = "bienvenida_admin.html"
		// }

		if (localStorage.tipo_usuario==38 || localStorage.tipo_usuario==39)  {
			window.location = "bienvenida_doc.html";
		}

	}
});

function iniciarSesion(){
	var id_colegio = $('#id_colegio').val()
	var id_usuario = $('#id_usuario').val()
	var clave = $('#clave').val()

	//var id_usuario = localStorage.getItem("id_usuario");

	$.ajax({
	url: URL_BASE+'/index/iniciar',
	type: 'POST',
	dataType: 'json',
	data: {'id_colegio':id_colegio, 'txt_usu':id_usuario, 'txt_con':clave, mobile:true },
	beforeSend: function() {
    	$('.loader').html("<img src='img/cargando.svg' />");
    },
	})
	.done(function(datos) {
		if (datos.estado==1) {
			localStorage.setItem("id_usuario", datos.id_usuario);
			localStorage.setItem("tipo_usuario", datos.tipo_usuario);

			if (datos.tipo_usuario == 20) {
				//window.location = "bienvenida_est.html"
			}else if(datos.tipo_usuario == 38 || datos.tipo_usuario == 39){
				window.location = "bienvenida_doc.html"
			}else{
				//window.location = "bienvenida_admin.html"
			}
			
		}else{
			alert("Usuario y/o contraseña incorrecto")
			// navigator.notification.alert(
   //              "Usuario y/o contraseña incorrecto",  // message
   //              function(){ },         // callback
   //              'Error',            // title
   //              'Aceptar'                  // buttonName
   //          );
			// window.location = "index.html"
		}
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		$('.loader').html("");
		console.log("complete");
	});

}
