$(document).ready(function () {
    listarSedesUsuario('page_listado_estudiante_sede')
    obtenerDatosUsuario();

    console.log("tipo_usuario :"+localStorage.getItem("tipo_usuario"))
    console.log("id_usuario :"+localStorage.getItem("id_usuario"))

    if (localStorage.getItem("tipo_usuario")==39) {
        $('.dir-grupo').removeClass('hidden').addClass('visible');
    }

});

function cargarEstudiantesPorSede(id_sede){

    if (id_sede == "") {
        alert("Seleccione salon")
        return false;
    }

    $("#page_listado_estudiante_sede_rta").html("")
    
	$.ajax({
	    url: URL_BASE+'/api/cargarEstudiantesPorSede',
	    type: 'POST',
	    dataType: 'json',
        data: {id_sede: id_sede},
        beforeSend: function() {
		    $('#page_listado_estudiante_sede .loader').html("<img src='img/cargando.svg'/>");
		},
	})
  	
  	.done(function(datos) {
        // console.log(datos.nombres_usuarios)
        // console.log(datos.apellidos_usuarios)
        var tabla ='';
        tabla += '<table class="lista">';
        tabla += '<tr>'
        tabla += '<th class="tac">No.</th>'
        //tabla += '<th class="tac">Identificación</th>'
        tabla += '<th class="tac">Apellidos y Nombres</th>'
        tabla += '<th class="tac">Celular</th>'
        console.log(datos)
        for (var i = 0; i < datos.length; i++) {
            console.log(datos[i].apellidos_usuario)
            console.log(datos[i].apellidos_usuario)

            tabla += '<tr>'
            tabla += '<td>'+(i+1)+'</td>'
            //tabla += '<td class="tal">'+datos[i].numero_documento+'</td>'
            tabla += '<td>'+datos[i].apellidos_usuario.toUpperCase()+' '+datos[i].nombres_usuario.toUpperCase()+ '</td>'
            tabla += '<td class="text-center">'+datos[i].celular_madre+' - '+datos[i].celular_madre+ '</td>'
            
            tabla += '</tr>'; 

        }

        tabla += '</table>'; 

        $("#page_listado_estudiante_sede_rta").html(tabla)
        
       
	})
	.fail(function() {
		$('#page_listado_estudiante_sede .loader').html("<div class='offline'><h6>Ha ocurrido un error</h6><p>Comprueba tu conexion y vuelve a intentarlo<p><input type='button' value='INTENTAR DE NUEVO' onclick='cargarEstudiantesPorSede("+id_sede+")'></div>");
		console.log("error");
	})
	.always(function() {
		console.log("complete");
		$('#page_listado_estudiante_sede .loader').html("");
	});
  	

}


function obtenerDatosUsuario(){
    
    var id_usuario = localStorage.getItem("id_usuario")
    $.ajax({
       url: URL_BASE+'/api/obtenerDatosUsuario',
       type: 'GET',
       dataType: 'json',
       data: {'id_usuario':id_usuario},
   })
   .done(function(datos) {
       console.log('obtenerDatosUsuario');
       console.log(datos)
       $('#nombre_usuario_menu').text(datos.nombres_usuario + ' ' + datos.apellidos_usuario)
       $('#email_usuario_menu').text(datos.correo_usuario)

       console.log("success");
   })
   .fail(function() {
       console.log("error");
   })
   .always(function() {
       console.log("complete");
   });
}